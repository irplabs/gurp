import { combineReducers } from 'redux';

import authentication from './authentication';
import ui from './ui';

export default combineReducers({
    [authentication.constants.NAME]: authentication.reducer,
    [ui.constants.NAME]: ui.reducer,
});