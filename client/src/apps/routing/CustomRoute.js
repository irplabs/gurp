import React, { Fragment } from 'react';
import { Route, Redirect } from 'react-router';
import { connect } from 'react-redux';

import ui from '../ui';

const mapStateToProps = (state, props) => ({
    isloggedIn: (state.authentication.token != "")   //add proper login-check, current one is noob's way
});

export default connect(mapStateToProps)(({ component: Component, layout, isloggedIn, generic, ...rest }) => (
    <Route {...rest} render={(props) => (
        (generic || isloggedIn)
            ? (
                <ui.Wrapper>
                    <ui.Layout layout> {/* Load Layout Changing Panel */}
                        <Component {...props} />  {/* Loads component requiring logged-in state */}
                    </ui.Layout>
                </ui.Wrapper>
            )
            : <Redirect to="/login" />
    )} />
));
