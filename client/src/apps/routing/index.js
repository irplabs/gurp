import React, { Suspense } from 'react';
import { Switch, Redirect, Route } from 'react-router';

import CustomRoute from './CustomRoute';
import home from '../home';
import authentication from '../authentication';

export default () => (
    <Suspense fallback={<div>Loading...</div>}>
        <Switch>
            <Route exact path="/" render={() => <Redirect push to='/home' />} />
            <CustomRoute generic path="/home" component={() => <home.Index /> } layout={home.layoutConfig} />
            <CustomRoute generic path="/login" component={() => <authentication.Index layout={authentication.layoutConfig} />} />
        </Switch>
    </Suspense>
);