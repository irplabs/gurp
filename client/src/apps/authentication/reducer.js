import C from './actionTypes';
import { combineReducers } from 'redux';

const username = (state='', action) => {
    switch(action.type) {
        case C.LOGIN.RESPONSE.SUCCESS:
            return action.data.username;
        case C.LOGOUT.SUCCESS:
            return '';
        default:
            return state;
    }
}

const token = (state='', action) => {
    switch(action.type) {
        case C.LOGIN.RESPONSE.SUCCESS:
            return action.data.token;
        case C.LOGOUT.SUCCESS:
            return '';
        default:
            return state;
    }
}

const group = (state='', action) => {
    switch(action.type) {
        case C.LOGIN.RESPONSE.SUCCESS:
            return action.data.group;
        case C.LOGOUT.SUCCESS:
            return '';
        default:
            return state;
    }
}

export default combineReducers({
    username,
    token,
    group,
});