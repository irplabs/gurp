import C from './actionTypes';

const login = (credentials) => ({ type: C.LOGIN.REQUEST.INITIATE, credentials });

export { login };