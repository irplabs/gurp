import React, { lazy } from 'react';

import * as constants from './constants';
import reducer from './reducer';
import * as selectors from './selectors';
import sagas from './sagas';
import layoutConfig from './layoutConfig';

const Index = lazy(() => import('./pages'));

export default { constants, reducer, selectors, sagas, Index, layoutConfig };