import { take, call, put, race, delay } from 'redux-saga/effects';

import C from './actionTypes';
import { history, api } from '../../services';

function authorize(credentials) {
    return api.post('api-token-auth/', credentials).then(response => response.data).catch(error => { throw error });
}

//perform login
function* login() {
    const { credentials } = yield take(C.LOGIN.REQUEST.INITIATE);

    try {
        const { apiRequest, timeout } = yield race({
                                                    apiRequest: call(authorize, credentials),
                                                    timeout: delay(5000)       //add 5sec delay for login call
                                                });

        if (timeout)
            yield put({ type: C.LOGIN.REQUEST.TIMEOUT });
        else {
            const { status, token, group } = apiRequest;
            const data = { username: credentials.username, group, token };

            if (status == 'LOGIN_SUCCESS') {
                yield put({ type: C.LOGIN.RESPONSE.SUCCESS, data });
                yield history.push('/login');
            }
            else {
                yield put({ type: C.LOGIN.RESPONSE.FAILED });
            }
        }
    }
    catch(error) {
        yield put({ type: C.LOGIN.ERROR, data: error.toString() });
    }
}

//perform logout
function* logout() {
    yield take(C.LOGOUT.REQUEST);
    localStorage.removeItem('login-data');
    yield put({ type: C.LOGOUT.SUCCESS });
}

export default { login, logout };