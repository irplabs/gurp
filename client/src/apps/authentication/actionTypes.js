export default {
    LOGIN: {
        REQUEST: {
            INITIATE: "INITIATE",    //initiate request
            TIMEOUT: "TIMEOUT",    //force-full timeout
        },
        RESPONSE: {
            SUCCESS: "SUCCESS",   //successful login
            FAILED: "FAILED",     //target end-point was hit but failed (maybe invalid credentials)
        },
        ERROR: "ERROR",           //any kind of error that might have occured (network errors)
    },  
    LOGOUT: {
        REQUEST: "REQUEST",       //perform request
        SUCCESS: "SUCCESS",       //successful logout
        FAILURE: "FAILURE",       //logout failure
    }
};