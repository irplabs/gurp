import React, { Fragment } from 'react';
import { Router } from 'react-router-dom';
import { connect } from 'react-redux';

import Routing from './routing';
import { history } from '../services';

import CssBaseline from '@material-ui/core/CssBaseline';
import ui from './ui';

const mapStateToProps = (state, props) => ({});

const mapDispatchToProps = dispatch => ({});

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: ''
        }
    }

    render() {
        return (
            <Router history={history}>
                <ui.ThemeProvider>
                    <Fragment>
                        {/* CssBaseline is a Material-UI component that fixes some inconsistencies across browsers and devices while providing slightly more opinionated resets to common HTML elements. For more visit : https://material-ui.com/style/css-baseline/ */}
                        <CssBaseline />
                        
                        <Routing />
                    </Fragment>
                </ui.ThemeProvider>
            </Router>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
    