import C from './actionTypes';

const changeTheme = (theme) => ({ type: C.CHANGE_THEME, payload: theme });

export { changeTheme };