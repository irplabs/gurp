import React from 'react';
import { withStyles, LinearProgress, Fade } from '@material-ui/core';
import { SnackbarProvider, withSnackbar } from 'notistack';

const styles = theme => ({
    root: {
        position: 'relative',
    },
    noOverflow: {
        overflow: 'hidden',
    },
    linearIndeterminateRoot: {
        position: 'fixed',
        top: 0,
        left: 0,
        flexGrow: 1,
        width: '100%',
        zIndex: 2100,

        [theme.breakpoints.down('md')] : {
            height: '3px', // default value is 4px
        }
    },
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 2000,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        width: '100vw',
        height: '100vh',
    },
});

const Wrapper = withSnackbar(({ classes, children, enqueueSnackbar }) => {
    return (
        <div className={classes.root}>
            <LinearProgress classes={{ 
                root: classes.linearIndeterminateRoot,
            }}/>

            <Fade in={true} timeout={10}>
                <div className={classes.overlay}></div>
            </Fade>

            {children}
        </div>
    )
});

const wrapperWithSnackbar = ({ ...props}) => <SnackbarProvider maxSnack={3}>
    <Wrapper {...props} />
</SnackbarProvider>

export default withStyles(styles)(wrapperWithSnackbar);