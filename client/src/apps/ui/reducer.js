import C from './actionTypes';
import { THEME_TYPES } from './constants';
import { combineReducers } from 'redux';

const theme = (state=THEME_TYPES.DARK, action) => {
    switch(action.type) {
        case C.CHANGE_THEME:
            return action.payload;
        default:
            return state;
    }
}

export default combineReducers({
    theme
});