import { createMuiTheme } from '@material-ui/core';
import { blue, red } from '@material-ui/core/colors';

export const light = createMuiTheme({
    palette: {
        primary: blue,
    },
    typography: {
        useNextVariants: true,
    }
})

export const dark = createMuiTheme({
    palette: {
        primary: red,
    },
    typography: {
        useNextVariants: true,
    }
})