import React from 'react';

import { history } from '../../../services';

export default () => <button onClick={() => history.push('/login')}>Go to Login</button>