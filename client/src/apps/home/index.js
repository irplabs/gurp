import React, { lazy } from 'react';

import layoutConfig from './layoutConfig';

const Index = lazy(() => import('./pages'));

export default { layoutConfig, Index };