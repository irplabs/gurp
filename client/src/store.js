import { createStore, applyMiddleware, compose } from 'redux';
import multi from 'redux-multi';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { reducer, saga } from './apps';

const persistConfig = {
    key: 'root',
    storage,
}

const sagaMiddleWare = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedReducer = persistReducer(persistConfig, reducer);

let store = createStore(
    persistedReducer,
    composeEnhancers(
        applyMiddleware(
            multi,
            sagaMiddleWare
        ),
    )
);

let persistor = persistStore(store);

sagaMiddleWare.run(saga);

export { store, persistor };