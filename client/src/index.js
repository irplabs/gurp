import { Provider } from 'react-redux';
import { render } from 'react-dom';
import React from 'react';
import { PersistGate } from 'redux-persist/integration/react';

import { App } from './apps';
import { store, persistor } from './store';

render(
    <Provider store={store}>
        <PersistGate loading={<div>Still Loading</div>} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>,
    document.getElementById('app')
)