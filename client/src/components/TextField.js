import React from 'react';
import { TextField, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

import { ThemeContext } from './theme';

const styles = theme => ({
    textField: {

    }
});

class ThemedTextField extends React.Component {
    render() {
        const { children, classes, ...restProps } = this.props;
        let theme = this.context;
        return (
            <TextField className={classes.textField} {...restProps} />
        )
    }
}
ThemedTextField.contextType = ThemeContext;

ThemedTextField.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(ThemedTextField);