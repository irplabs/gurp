import React from 'react';
import { Button, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

import { ThemeContext } from './theme';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    }
});

class ThemedButton extends React.Component {
    render() {
        const { children, classes, ...restProps } = this.props;
        let theme = this.context;
        return (
            <Button
                className={classes.button} 
                // style={{ backgroundColor: theme.background }} 
                {...restProps}
            >
                {children}
            </Button>
        )
    }
}

ThemedButton.contextType = ThemeContext;

ThemedButton.propTypes = {
    classes: PropTypes.object.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]).isRequired
};

export default withStyles(styles)(ThemedButton);