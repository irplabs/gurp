import React from 'react';
import { Drawer, withStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

import { ThemeContext } from './theme';

const styles = theme => ({
    drawer: {

    }
});

class ThemedDrawer extends React.Component {
    render() {
        const { children, classes, ...restProps } = this.props;
        let theme = this.context;
        return (
            <Drawer
                className={classes.drawer}
                {...restProps}
            >
                {children}
            </Drawer>
        );
    }
}
ThemedDrawer.contextType = ThemeContext;

ThemedDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
    ]).isRequired,
}

export default withStyles(styles)(ThemedDrawer);