import React from 'react';

export const themes = {
    light: {
        background: 'red'
    },
    dark: {
        background: 'blue'
    }
}

export const ThemeContext = React.createContext(
    themes.dark
);