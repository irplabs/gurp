from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from utility.models import*
from rest_framework import*

class DepartmentSerializer(ModelSerializer):

	class Meta:
		model = Department
		fields = "__all__"


class SessionSerializer(ModelSerializer):

	class Meta:
		model = Session
		fields = "__all__"

class ProgramTypeSerializer(ModelSerializer):

	class Meta:
		model = ProgramType
		fields = "__all__"

class ProgramSerializer(ModelSerializer):

	department = DepartmentSerializer(read_only=True)
	program_type = ProgramTypeSerializer(read_only=True)

	class Meta:
		model = Program
		fields = "__all__"


class BatchSerializer(ModelSerializer):

	program = ProgramSerializer(read_only=True)
	session = SessionSerializer(read_only=True)

	class Meta:
		model = Batch
		fields = "__all__"		


class CategorySerializer(ModelSerializer):

	class Meta:
		model = Category
		fields = "__all__"	

class SeatMatrixSerializer(ModelSerializer):

	batch = BatchSerializer(read_only=True)
	category = CategorySerializer(read_only=True)

	class Meta:
		model = SeatMatrix
		fields = "__all__"	

class AddressSerializer(ModelSerializer):

	class Meta:
		model = Address
		fields = "__all__"	

class PersonSerializer(ModelSerializer):

	class Meta:
		model = Person
		fields = "__all__"	

class EducationBoardSerializer(ModelSerializer):

	class Meta:
		model = EducationBoard
		fields = "__all__"	

class AssesmentTypeSerializer(ModelSerializer):

	class Meta:
		model = AssesmentType
		fields = "__all__"	

class HighschoolRecordSerializer(ModelSerializer):

	assesment_type = AssesmentTypeSerializer(read_only=True)
	board = EducationBoardSerializer(read_only=True)

	class Meta:
		model = HighschoolRecord
		fields = "__all__"	

class IntermediateRecordSerializer(ModelSerializer):

	assesment_type = AssesmentTypeSerializer(read_only=True)
	board = EducationBoardSerializer(read_only=True)

	class Meta:
		model = IntermediateRecord
		fields = "__all__"	

class PositionTypeSerializer(ModelSerializer):

	class Meta:
		model = PositionType
		fields = "__all__"

class ExamSerializer(ModelSerializer):

	class Meta:
		model = Exam
		fields = "__all__"

class QualifyingExamSerializer(ModelSerializer):

	assesment_type = AssesmentTypeSerializer(read_only=True)
	position_type = PositionTypeSerializer(read_only=True)
	exam = ExamSerializer(read_only=True)

	class Meta:
		model = QualifyingExam
		fields = "__all__"

class GuardianSerializer(ModelSerializer):

	father = PersonSerializer(read_only=True)
	mother = PersonSerializer(read_only=True)

	class Meta:
		model = Guardian
		fields = "__all__"

class LocalGuardianSerializer(ModelSerializer):

	person = PersonSerializer(read_only=True)
	address = AddressSerializer(read_only=True)

	class Meta:
		model = LocalGuardian
		fields = "__all__"

class GenderSerializer(ModelSerializer):

	class Meta:
		model = Gender
		fields = "__all__"

class SemesterSerializer(ModelSerializer):

	class Meta:
		model = Semester
		fields = "__all__"

class SectionSerializer(ModelSerializer):

	class Meta:
		model = Section
		fields = "__all__"

class ElectiveSlotSerializer(ModelSerializer):

	class Meta:
		model = ElectiveSlot
		fields = "__all__"

class AddonSlotSerializer(ModelSerializer):

	class Meta:
		model = AddonSlot
		fields = "__all__"


class ElectiveBasketSerializer(ModelSerializer):

	class Meta:
		model = ElectiveBasket
		fields = "__all__"

class AddonBasketSerializer(ModelSerializer):

	class Meta:
		model = AddonBasket
		fields = "__all__"

class DropReasonSerializer(ModelSerializer):

	class Meta:
		model = DropReason
		fields = "__all__"