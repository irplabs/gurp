# Generated by Django 2.1.5 on 2019-01-21 11:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utility', '0009_auto_20190121_1140'),
    ]

    operations = [
        migrations.CreateModel(
            name='Exam',
            fields=[
                ('exam_id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': 'Exam',
                'verbose_name_plural': 'Exams',
            },
        ),
    ]
