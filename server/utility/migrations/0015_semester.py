# Generated by Django 2.1.5 on 2019-01-21 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utility', '0014_gender'),
    ]

    operations = [
        migrations.CreateModel(
            name='Semester',
            fields=[
                ('number', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Semesters',
                'verbose_name': 'Semester',
            },
        ),
    ]
