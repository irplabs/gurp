from __future__ import unicode_literals

from django.shortcuts import render


from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from utility.models import*
from utility.serializers import*
from utility.views import*
from student.models import*
from student.serializers import*
from course.models import*
from course.serializers import*
from course.models import*
from course.serializers import*
from result.models import*
from result.serializers import*
from faculty.models import*
import hashlib
import csv

def resetpassword():
	users = User.objects.all()
	for user in users:
		user.set_password('unierp@123')
		user.save()

	print("Success")


def importcourse():

	with open("course.csv", 'r') as csvfile: 
		# creating a csv reader object 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			course_id = row[0]
			name = row[1]
			l = row[2]
			t = row[3]
			p = row[4]
			credits = row[5]
			dept_id = row[6]
			department = Department.objects.get(department_id=dept_id)
			course = Course.objects.create(course_id=course_id,
				name=name,
				l=l,
				t=t,
				p=p,
				credits=credits,
				department=department)


	print("Success")

def importelectiveoffered():

	with open("electiveoffered.csv", 'r') as csvfile: 
		# creating a csv reader object 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			course_id = row[0]
			course = Course.objects.get(course_id=course_id)
			number = row[4]
			semester = Semester.objects.get(number=number)
			program_id = row[5]
			program = Program.objects.get(program_id=program_id)
			year = 2018
			slot_name = row[1]
			slot = ElectiveSlot.objects.get(name=slot_name)
			basket_id = row[2]
			basket = ElectiveBasket.objects.get(basket_id=basket_id)
			batch = Batch.objects.get(program=program,admission_year=year)
			if row[6] == 'JL-2018':
				session_id = 'JUL-18'
			else :
				session_id = 'JAN-19'

			session = Session.objects.get(session_id=session_id)
			seats = row[3]

			
			course = ElectiveCourseOffered.objects.get_or_create(course=course,
				batch=batch,
				session=session,
				semester=semester,
				active=True,
				slot = slot,
				basket = basket,
				seats=seats)


	print("Success")

def importaddonoffered():

	with open("addonoffered1.csv", 'r') as csvfile: 
		# creating a csv reader object 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			course_id = row[0]
			course = Course.objects.get(course_id=course_id)
			slot_name = row[1]
			slot = AddonSlot.objects.get(name=slot_name)
			basket_name = row[2]
			basket = AddonBasket.objects.get(name=basket_name)
			seats = row[3]
			number = row[4]
			semester = Semester.objects.get(number=number)
			program_id = row[5]
			program = Program.objects.get(program_id=program_id)
			year = 2018
			batch = Batch.objects.get(program=program,admission_year=year)
			if row[6] == 'JL-2018':
				session_id = 'JUL-18'
			else :
				session_id = 'JAN-19'

			session = Session.objects.get(session_id=session_id)
			course = AddonCourseOffered.objects.get_or_create(course=course,
				batch=batch,
				session=session,
				semester=semester,
				active=True,
				slot = slot,
				basket = basket,
				seats=seats)


	print("Success")

def importcourseoffered():

	with open("courseoffered.csv", 'r') as csvfile: 
		# creating a csv reader object 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			course_id = row[0]
			course = Course.objects.get(course_id=course_id)
			number = row[1]
			semester = Semester.objects.get(number=number)
			program_id = row[2]
			program = Program.objects.get(program_id=program_id)
			year = 2018
			batch = Batch.objects.get(program=program,admission_year=year)
			if row[3] == 'JL-2018':
				session_id = 'JUL-18'
			else :
				session_id = 'JAN-19'

			session = Session.objects.get(session_id=session_id)

			
			course = CoreCourseOffered.objects.get_or_create(course=course,
				batch=batch,
				session=session,
				semester=semester,
				active=True)


	print("Success")


def importstudents():

	with open("media/data/students.csv", 'r') as csvfile:
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			user_name = row[0]
			username = user_name.lower()
			user = User.objects.filter(username=username)
			if not user.exists():
				user = User.objects.create_user(username=username,password='##**aviral2018**##')
				group = Group.objects.get(name='Student')
				user.groups.add(group)
				student_id = row[1]
				email = row[2]
				name = row[3]
				phone = row[4]
				person = Person.objects.create(name=name,email=email,phone=phone)
				aadhar = row[5]
				active = row[6]
				program_id = row[7]
				program = Program.objects.get(program_id=program_id)
				year = row[8]
				batch = Batch.objects.get(program=program,admission_year=year)
				dob = row[9]
				number = row[10]
				semester = Semester.objects.get(number=number)
				sec = row[11]
				section = Section.objects.get(section=sec)
				category_id = row[12]
				category = Category.objects.get(category_id=category_id)
				gender = Gender.objects.get(name=row[13])

				student = Student.objects.get_or_create(user=user,
					admission_id=student_id,
					person=person,
					aadhar=aadhar,
					active=active,
					batch=batch,
					birth_date=dob,
					semester=semester,
					section=section,
					category=category,
					gender=gender)

	print("Success")

def importfaculties():

	with open("media/data/faculties.csv", 'r') as csvfile:
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			user_name = row[0]
			username = user_name.lower()
			user = User.objects.filter(username=username)
			if not user.exists():
				user = User.objects.create_user(username=username,password='##**aviral2018**##')
				group = Group.objects.get(name='Faculty')
				user.groups.add(group)
				faculty_id = row[1]
				prefix = row[2]
				email = row[3]
				name = row[4]
				phone = row[5]
				person = Person.objects.create(name=name,email=email,phone=phone)
				active = row[6]
				interest = row[7]
				gender_id = row[8]
				gender = Gender.objects.get(gender_id=gender_id)
				dept_id = row[9]
				department = Department.objects.get(department_id=dept_id)

				faculty = Faculty.objects.get_or_create(user=user,
					faculty_id=faculty_id,
					person=person,
					active=active,
					prefix=prefix,
					department=department,
					interest=interest,
					gender=gender)

	print("Success")

def importenrolled():

	with open("media/data/enrolled.csv", 'r') as csvfile: 
		# creating a csv reader object 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			print(row)
			admission_id = row[0]
			student = Student.objects.get(admission_id=admission_id)
			course_id = row[1]
			course = Course.objects.get(course_id=course_id)
			type_id = row[2]
			course_type = CourseType.objects.get(type_id=type_id)
			session_id = row[3]
			session = Session.objects.get(session_id=session_id)
			if session_id == 'JUL-18':
				date = '2018-08-05'
			else :
				date = '2019-01-20'
			number = row[4]
			semester = Semester.objects.get(number=number)
			c1_score = row[5]
			if c1_score != 'NA':
				c1 = ComponentResult.objects.create(score=c1_score,present=True)
			else :
				c1 = ComponentResult.objects.create(present=False)
			c2_score = row[6]
			if c2_score != 'NA':
				c2 = ComponentResult.objects.create(score=c2_score,present=True)
			else :
				c2 = ComponentResult.objects.create(present=False)
			c3_score = row[7]
			if c3_score != 'NA':
				c3 = ComponentResult.objects.create(score=c3_score,present=True)
			else :
				c3 = ComponentResult.objects.create(present=False)
			total = row[8]
			if total == 'NA':
				total = 0.00
			cgpi = row[9]
			if cgpi == 'NA':
				cgpi = 0.00

			result = CourseResult.objects.create(c1=c1,
				c2=c2,
				c3=c3,
				total=total,
				cgpi=cgpi)

			enrolled = EnrolledCourse.objects.get_or_create(student=student,
				course=course,
				session=session,
				semester=semester,
				course_type=course_type,
				date=date,
				result=result)


	print("Success")



def results():

	with open("media/data/results.csv", 'r') as csvfile: 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			print(row)
			admission_id = row[0]
			student = Student.objects.get(admission_id=admission_id)
			number = row[1]
			semester = Semester.objects.get(number=number)
			data = row[0] + " " + semester.name + " " + row[3]
			print(data)
			data = data.encode()
			h = hashlib.sha256(data)
			fingerprint = h.hexdigest()
			print(fingerprint)
			session_id = "JUL-18"
			session = Session.objects.get(session_id=session_id)
			result = SemesterResult.objects.get_or_create(student=student,
				semester=semester,
				session=session,
				credits=row[2],
				sgpi=row[3],
				addon_credits=row[4],
				result_date = row[5],
				fingerprint=fingerprint,
				rank = row[6])

	print("Success")

def facultycourses():
	with open("media/data/facultycourses.csv", 'r') as csvfile: 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			print(row)
			course_id = row[0]
			course = Course.objects.get(course_id=course_id)
			cordinator_id = row[1]
			cordinator = Faculty.objects.get(faculty_id=cordinator_id)
			faculty_id = row[2]
			faculty = Faculty.objects.get(faculty_id=faculty_id)
			session_id = row[3]
			session = Session.objects.get(session_id=session_id)
			section_id = row[4]
			section = Section.objects.get(section=section_id)

			facultycourse = FacultyCourse.objects.create(course=course,
				cordinator=cordinator,
				faculty=faculty,
				session=session,
				section=section)

	print("Success")

def dropped():
	with open("media/data/dropped.csv", 'r') as csvfile: 
		csvreader = csv.reader(csvfile) 
		for row in csvreader:
			print(row)
			student_id = row[0]
			student = Student.objects.get(admission_id=student_id)
			course_id = row[1]
			course = Course.objects.get(course_id=course_id)
			session_id = 'JUL-18'
			session = Session.objects.get(session_id=session_id)
			enrolled = EnrolledCourse.objects.get(student=student,course=course,session=session)
			drop_id = row[2]
			date = row[3]
			reason = DropReason.objects.get(drop_id=drop_id)
			dropped = DropCourse.objects.get_or_create(enrolled=enrolled,
				reason=reason,
				date = date)

	print("Success")


