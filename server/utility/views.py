# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from utility.models import*
from utility.serializers import*
from utility.authenticate import*
from utility.session import*
from server.settings import SECRET_KEY
import jwt

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
    	try:
    		serializer = self.serializer_class(data=request.data,context={'request': request })
    		serializer.is_valid(raise_exception=True)

    		user = serializer.validated_data['user']
    		ip = get_client_ip(request)
    		jwt_token = jwt.encode({ 'username': user.username,'ip': ip },SECRET_KEY,algorithm = 'HS256').decode('utf-8')

    		group = Group.objects.get(user=user)
    		return JsonResponse({ 'status': 'LOGIN_SUCCESS', 'token': jwt_token, 'group': group.name })

    	except Exception as e:
    		print(str(e))
    		return Response({ 'status': 'LOGIN_FAILED' })

class DepartmentListView(ListAPIView):
	queryset = Department.objects.all()
	serializer_class = DepartmentSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = DepartmentSerializer(queryset,many=True)
		return Response(serializer.data)


class SessionListView(ListAPIView):
	queryset = Session.objects.all()
	serializer_class = SessionSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = SessionSerializer(queryset,many=True)
		return Response(serializer.data)

class SessionDetailView(RetrieveAPIView):
	queryset = Session.objects.all()
	serializer_class = SessionSerializer

class ProgramTypeListView(ListAPIView):
	queryset = ProgramType.objects.all()
	serializer_class = ProgramTypeSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = ProgramTypeSerializer(queryset,many=True)
		return Response(serializer.data)


class ProgramListView(ListAPIView):
	queryset = Program.objects.all()
	serializer_class = ProgramSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = ProgramSerializer(queryset,many=True)
		return Response(serializer.data)

class BatchListView(ListAPIView):
	queryset = Batch.objects.all()
	serializer_class = BatchSerializer

	def list(self,request):
		access_group = ['STUDENT']
		status = isAuthenticated(request,access_group)
		if status != 'SUCCESS':
			return Response({ 'status': status })


		queryset = self.get_queryset()
		serializer = BatchSerializer(queryset,many=True)
		response = Response(serializer.data)
		response.data['status'] = status
		return response

class CategoryListView(ListAPIView):
	queryset = Category.objects.all()
	serializer_class = CategorySerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = CategorySerializer(queryset,many=True)
		return Response(serializer.data)

class SeatMatrixListView(ListAPIView):
	queryset = SeatMatrix.objects.all()
	serializer_class = SeatMatrixSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = SeatMatrixSerializer(queryset,many=True)
		return Response(serializer.data)

class AddressListView(ListAPIView):
	queryset = Address.objects.all()
	serializer_class = AddressSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = AddressSerializer(queryset,many=True)
		return Response(serializer.data)

class PersonListView(ListAPIView):
	queryset = Person.objects.all()
	serializer_class = PersonSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = PersonSerializer(queryset,many=True)
		return Response(serializer.data)

class EducationBoardListView(ListAPIView):
	queryset = EducationBoard.objects.all()
	serializer_class = EducationBoardSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = EducationBoardSerializer(queryset,many=True)
		return Response(serializer.data)

class AssesmentTypeListView(ListAPIView):
	queryset = AssesmentType.objects.all()
	serializer_class = AssesmentTypeSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = AssesmentTypeSerializer(queryset,many=True)
		return Response(serializer.data)

class HighschoolRecordListView(ListAPIView):
	queryset = HighschoolRecord.objects.all()
	serializer_class = HighschoolRecordSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = HighschoolRecordSerializer(queryset,many=True)
		return Response(serializer.data)

class IntermediateRecordListView(ListAPIView):
	queryset = IntermediateRecord.objects.all()
	serializer_class = IntermediateRecordSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = IntermediateRecordSerializer(queryset,many=True)
		return Response(serializer.data)

class PositionTypeListView(ListAPIView):
	queryset = PositionType.objects.all()
	serializer_class = PositionTypeSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = PositionTypeSerializer(queryset,many=True)
		return Response(serializer.data)


class ExamListView(ListAPIView):
	queryset = Exam.objects.all()
	serializer_class = ExamSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = ExamSerializer(queryset,many=True)
		return Response(serializer.data)

class QualifyingExamListView(ListAPIView):
	queryset = QualifyingExam.objects.all()
	serializer_class = QualifyingExamSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = QualifyingExamSerializer(queryset,many=True)
		return Response(serializer.data)

class GuardianListView(ListAPIView):
	queryset = Guardian.objects.all()
	serializer_class = GuardianSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = GuardianSerializer(queryset,many=True)
		return Response(serializer.data)


class LocalGuardianListView(ListAPIView):
	queryset = LocalGuardian.objects.all()
	serializer_class = LocalGuardianSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = LocalGuardianSerializer(queryset,many=True)
		return Response(serializer.data)

class LocalGuardianListView(ListAPIView):
	queryset = LocalGuardian.objects.all()
	serializer_class = LocalGuardianSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = LocalGuardianSerializer(queryset,many=True)
		return Response(serializer.data)

class GenderListView(ListAPIView):
	queryset = Gender.objects.all()
	serializer_class = GenderSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = GenderSerializer(queryset,many=True)
		return Response(serializer.data)

class SemesterListView(ListAPIView):
	queryset = Semester.objects.all()
	serializer_class = SemesterSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = SemesterSerializer(queryset,many=True)
		return Response(serializer.data)

class SectionListView(ListAPIView):
	queryset = Section.objects.all()
	serializer_class = SectionSerializer

	def list(self,request):

		if not isAuthenticated(request):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = SectionSerializer(queryset,many=True)
		return Response(serializer.data)


class ElectiveSlotListView(ListAPIView):
	queryset = ElectiveSlot.objects.all()
	serializer_class = ElectiveSlotSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = ElectiveSlotSerializer(queryset,many=True)
		return Response(serializer.data)

class AddonSlotListView(ListAPIView):
	queryset = AddonSlot.objects.all()
	serializer_class = AddonSlotSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = AddonSlotSerializer(queryset,many=True)
		return Response(serializer.data)