# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import date
from decimal import Decimal

# Create your models here.

class Department(models.Model):

	# DATABASE FIELDS
	department_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)


	# META CLASS
	class Meta:
		verbose_name = 'Department'
		verbose_name_plural = 'Departments'

	# TO STRING METHOD
	def __str__(self):
		return (self.department_id) + "-" + (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

	# ABSOLUTE URL METHOD
	def get_absolute_url(self):
		return reverse('department_details', kwargs={'pk': self.department_id})

# Create your models here.

class Session(models.Model):

	# DATABASE FIELDS
	session_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)
	year = models.IntegerField(default=2018)
	start_date  = models.DateField(default=date.today)
	end_date = models.DateField(default=date.today)

	# META CLASS
	class Meta:
		verbose_name = 'Session'
		verbose_name_plural = 'Sessions'

	# TO STRING METHOD
	def __str__(self):
		return (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class ProgramType(models.Model):

	# DATABASE FIELDS
	program_type_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)

	# META CLASS
	class Meta:
		verbose_name = 'Program Type'
		verbose_name_plural = 'Program Types'

	# TO STRING METHOD
	def __str__(self):
		return (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something



class Program(models.Model):

	# DATABASE FIELDS
	program_id = models.CharField(max_length=10,primary_key=True)
	short_name = models.CharField(max_length=200)
	full_name = models.CharField(max_length=500)
	department = models.ForeignKey(Department,on_delete=models.CASCADE)
	program_type = models.ForeignKey(ProgramType,on_delete=models.CASCADE)
	min_duration = models.IntegerField()
	max_duration = models.IntegerField()
	total_credits = models.IntegerField()
	core_credits = models.IntegerField()
	elective_credits = models.IntegerField()


	# META CLASS
	class Meta:
		verbose_name = 'Program'
		verbose_name_plural = 'Programs'

	# TO STRING METHOD
	def __str__(self):
		return (self.short_name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


class Batch(models.Model):

	# DATABASE FIELDS
	program = models.ForeignKey(Program,on_delete=models.CASCADE)
	admission_year = models.IntegerField()
	session = models.ForeignKey(Session,on_delete=models.CASCADE)
	

	# META CLASS
	class Meta:
		verbose_name = 'Batch'
		verbose_name_plural = 'Batches'

	# TO STRING METHOD
	def __str__(self):
		return str(self.program) + "-" + str(self.session)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something



class Category(models.Model):

	# DATABASE FIELDS
	category_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=200)
	

	# META CLASS
	class Meta:
		verbose_name = 'Category'
		verbose_name_plural = 'Categories'

	# TO STRING METHOD
	def __str__(self):
		return (self.category_id)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


class SeatMatrix(models.Model):

	# DATABASE FIELDS
	batch = models.ForeignKey(Batch,on_delete=models.CASCADE)
	category = models.ForeignKey(Category,on_delete=models.CASCADE)
	seats = models.IntegerField()
	

	# META CLASS
	class Meta:
		verbose_name = 'Seat Matrix'
		verbose_name_plural = 'Seat Matrices'

	# TO STRING METHOD
	def __str__(self):
		return str(self.batch) + "-" + str(self.category) + "-" + str(self.seats)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


class Address(models.Model):

	# DATABASE FIELDS
	house_address = models.TextField(max_length=500)
	pincode = models.CharField(max_length=20)
	city = models.CharField(max_length=50)
	state = models.CharField(max_length=50)
	country = models.CharField(max_length=50)
	

	# META CLASS
	class Meta:
		verbose_name = 'Address'
		verbose_name_plural = 'Addresses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.city) + "-" + str(self.state)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class Person(models.Model):

	# DATABASE FIELDS
	name = models.CharField(max_length=200)
	email = models.EmailField(max_length=70,blank=True,unique=True)
	phone = models.CharField(max_length=20)

	# META CLASS
	class Meta:
		verbose_name = 'Person'
		verbose_name_plural = 'Persons'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name) + "-" + str(self.email)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class EducationBoard(models.Model):

	# DATABASE FIELDS
	board_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=200)

	# META CLASS
	class Meta:
		verbose_name = 'Education Board'
		verbose_name_plural = 'Education Boards'

	# TO STRING METHOD
	def __str__(self):
		return str(self.board_id) + "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class AssesmentType(models.Model):

	# DATABASE FIELDS
	assesment_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=200)

	# META CLASS
	class Meta:
		verbose_name = 'Assesment Type'
		verbose_name_plural = 'Assesment Types'

	# TO STRING METHOD
	def __str__(self):
		return str(self.assesment_id)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class HighschoolRecord(models.Model):

	# DATABASE FIELDS
	board = models.ForeignKey(EducationBoard,on_delete=models.CASCADE)
	assesment_type = models.ForeignKey(AssesmentType,on_delete=models.CASCADE)
	max_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	obtained_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	passing_year = models.IntegerField()

	# META CLASS
	class Meta:
		verbose_name = 'Highschool Record'
		verbose_name_plural = 'Highschool Records'

	# TO STRING METHOD
	def __str__(self):
		return str(self.board) + "-" + str(self.assesment_type) + "-" + str(self.passing_year) + "-" + str(self.max_score) + "->" + str(self.obtained_score) 
	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class IntermediateRecord(models.Model):

	# DATABASE FIELDS
	board = models.ForeignKey(EducationBoard,on_delete=models.CASCADE)
	assesment_type = models.ForeignKey(AssesmentType,on_delete=models.CASCADE)
	max_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	obtained_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	passing_year = models.IntegerField()

	# META CLASS
	class Meta:
		verbose_name = 'Intermediate Record'
		verbose_name_plural = 'Intermediate Records'

	# TO STRING METHOD
	def __str__(self):
		return str(self.board) + "-" + str(self.assesment_type) + "-" + str(self.passing_year) + "-" + str(self.max_score) + "->" + str(self.obtained_score) 
	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class PositionType(models.Model):

	# DATABASE FIELDS
	position_type_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=200)

	# META CLASS
	class Meta:
		verbose_name = 'Position Type'
		verbose_name_plural = 'Position Types'

	# TO STRING METHOD
	def __str__(self):
		return str(self.position_type_id)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class Exam(models.Model):

	# DATABASE FIELDS
	exam_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=200)

	# META CLASS
	class Meta:
		verbose_name = 'Exam'
		verbose_name_plural = 'Exams'

	# TO STRING METHOD
	def __str__(self):
		return str(self.exam_id)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.



class QualifyingExam(models.Model):

	# DATABASE FIELDS
	exam = models.ForeignKey(Exam,on_delete=models.CASCADE)
	assesment_type = models.ForeignKey(AssesmentType,on_delete=models.CASCADE)
	position_type = models.ForeignKey(PositionType,on_delete=models.CASCADE)
	position = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	max_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	obtained_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	qualifying_year = models.IntegerField()

	# META CLASS
	class Meta:
		verbose_name = 'Qualifying Exam'
		verbose_name_plural = 'Qualifying Exams'

	# TO STRING METHOD
	def __str__(self):
		return str(self.exam) + "-" + str(self.position)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class Guardian(models.Model):

	# DATABASE FIELDS
	father = models.ForeignKey(Person,on_delete=models.CASCADE,related_name="father")
	mother = models.ForeignKey(Person,on_delete=models.CASCADE,related_name="mother")

	
	# META CLASS
	class Meta:
		verbose_name = 'Guardian'
		verbose_name_plural = 'Guardians'

	# TO STRING METHOD
	def __str__(self):
		return str(self.father) + "-" + str(self.mother)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class LocalGuardian(models.Model):

	# DATABASE FIELDS
	person = models.ForeignKey(Person,on_delete=models.CASCADE,related_name="local_guardian")
	address = models.ForeignKey(Address,on_delete=models.CASCADE)

	
	# META CLASS
	class Meta:
		verbose_name = 'Local Guardian'
		verbose_name_plural = 'Local Guardians'

	# TO STRING METHOD
	def __str__(self):
		return str(self.person) + "-" + str(self.address)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.



class Gender(models.Model):

	# DATABASE FIELDS
	gender_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Gender'
		verbose_name_plural = 'Genders'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

class Semester(models.Model):

	# DATABASE FIELDS
	number = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Semester'
		verbose_name_plural = 'Semesters'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

class Section(models.Model):

	# DATABASE FIELDS
	section = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Section'
		verbose_name_plural = 'Sections'

	# TO STRING METHOD
	def __str__(self):
		return str(self.section)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

class ElectiveSlot(models.Model):

	# DATABASE FIELDS
	slot_id= models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Elective Slot'
		verbose_name_plural = 'Elective Slots'

	# TO STRING METHOD
	def __str__(self):
		return str(self.slot_id) + "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

class AddonSlot(models.Model):

	# DATABASE FIELDS
	slot_id= models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Addon Slot'
		verbose_name_plural = 'Addon Slots'

	# TO STRING METHOD
	def __str__(self):
		return str(self.slot_id) + "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class ElectiveBasket(models.Model):

	# DATABASE FIELDS
	basket_id= models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Elective Basket'
		verbose_name_plural = 'Elective Baskets'

	# TO STRING METHOD
	def __str__(self):
		return str(self.basket_id) + "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.

class AddonBasket(models.Model):

	# DATABASE FIELDS
	basket_id= models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=100)
	
	# META CLASS
	class Meta:
		verbose_name = 'Addon Basket'
		verbose_name_plural = 'Addon Baskets'

	# TO STRING METHOD
	def __str__(self):
		return str(self.basket_id) + "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class DropReason(models.Model):

	# DATABASE FIELDS
	drop_id= models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=150)
	
	# META CLASS
	class Meta:
		verbose_name = 'Drop Reason'
		verbose_name_plural = 'Drop Reasons'

	# TO STRING METHOD
	def __str__(self):
		return str(self.drop_id) + "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class DropCutoff(models.Model):

	# DATABASE FIELDS
	program = models.ForeignKey(Program,on_delete=models.CASCADE)
	c1_c2 = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	c3 = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	cgpi = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	sgpi = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	
	# META CLASS
	class Meta:
		verbose_name = 'Drop Cutoff'
		verbose_name_plural = 'Drop Cutoffs'

	# TO STRING METHOD
	def __str__(self):
		return str(self.program) + "-" + str(self.c1_c2) + "-" + str(self.c3) + "-" + str(self.cgpi) + "-" + str(self.sgpi)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.