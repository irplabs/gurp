# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import*
from import_export import resources


# Register your models here.

@admin.register(Department)
class DepartmentAdmin(ImportExportModelAdmin):
    pass

@admin.register(Session)
class SessionAdmin(ImportExportModelAdmin):
	pass

@admin.register(ProgramType)
class ProgramTypeAdmin(ImportExportModelAdmin):
	pass

@admin.register(Program)
class ProgramAdmin(ImportExportModelAdmin):
	pass

@admin.register(Batch)
class BatchAdmin(ImportExportModelAdmin):
	pass	

@admin.register(Category)
class CategoryAdmin(ImportExportModelAdmin):
	pass

@admin.register(SeatMatrix)
class SeatMatrixAdmin(ImportExportModelAdmin):
	pass

@admin.register(Address)
class AddressAdmin(ImportExportModelAdmin):
	pass

@admin.register(Person)
class PersonAdmin(ImportExportModelAdmin):
	pass

@admin.register(EducationBoard)
class EducationBoardAdmin(ImportExportModelAdmin):
	pass

@admin.register(AssesmentType)
class AssesmentTypeAdmin(ImportExportModelAdmin):
	pass

@admin.register(HighschoolRecord)
class HighschoolRecordAdmin(ImportExportModelAdmin):
	pass

@admin.register(IntermediateRecord)
class IntermediateRecordAdmin(ImportExportModelAdmin):
	pass

@admin.register(PositionType)
class PositionTypeAdmin(ImportExportModelAdmin):
	pass

@admin.register(Exam)
class ExamAdmin(ImportExportModelAdmin):
	pass

@admin.register(QualifyingExam)
class QualifyingExamAdmin(ImportExportModelAdmin):
	pass

@admin.register(Guardian)
class GuardianAdmin(ImportExportModelAdmin):
	pass

@admin.register(LocalGuardian)
class LocalGuardianAdmin(ImportExportModelAdmin):
	pass

@admin.register(Gender)
class GenderAdmin(ImportExportModelAdmin):
	pass

@admin.register(Semester)
class SemesterAdmin(ImportExportModelAdmin):
	pass

@admin.register(Section)
class SectionAdmin(ImportExportModelAdmin):
	pass

@admin.register(ElectiveSlot)
class ElectiveSlotAdmin(ImportExportModelAdmin):
	pass

@admin.register(AddonSlot)
class AddonSlotAdmin(ImportExportModelAdmin):
	pass

@admin.register(ElectiveBasket)
class ElectiveBasketAdmin(ImportExportModelAdmin):
	pass

@admin.register(AddonBasket)
class AddonBasketAdmin(ImportExportModelAdmin):
	pass

@admin.register(DropReason)
class DropReasonAdmin(ImportExportModelAdmin):
	pass

@admin.register(DropCutoff)
class DropCutoffAdmin(ImportExportModelAdmin):
	pass