from django.conf.urls import url
from django.urls import path, include
from django.contrib import admin
from utility.views import CustomAuthToken
from utility.views import*


urlpatterns = [

    url(r'^api-token-auth/', CustomAuthToken.as_view()),
    url(r'^api/departments/',DepartmentListView.as_view(),name='view-all'),
    url(r'^api/sessions/$',SessionListView.as_view(),name='view-all'),
    url(r'^api/sessions/(?P<pk>[\w\-]+)',SessionDetailView.as_view(),name='details'),
    url(r'^api/programtypes/',ProgramTypeListView.as_view(),name='view-all'),
    url(r'^api/programs/',ProgramListView.as_view(),name='view-all'),
    url(r'^api/batches/',BatchListView.as_view(),name='view-all'),
    url(r'^api/categories/',CategoryListView.as_view(),name='view-all'),
    url(r'^api/seat_matrices/',SeatMatrixListView.as_view(),name='view-all'),
    url(r'^api/addresses/',AddressListView.as_view(),name='view-all'),
    url(r'^api/persons/',PersonListView.as_view(),name='view-all'),
    url(r'^api/education_boards/',EducationBoardListView.as_view(),name='view-all'),
    url(r'^api/assesment_types/',AssesmentTypeListView.as_view(),name='view-all'),
    url(r'^api/highschool_records/',HighschoolRecordListView.as_view(),name='view-all'),
    url(r'^api/intermediate_records/',IntermediateRecordListView.as_view(),name='view-all'),
    url(r'^api/position_types/',PositionTypeListView.as_view(),name='view-all'),
    url(r'^api/exams/',ExamListView.as_view(),name='view-all'),
    url(r'^api/qualifying_exams/',QualifyingExamListView.as_view(),name='view-all'),
    url(r'^api/guardians/',GuardianListView.as_view(),name='view-all'),
    url(r'^api/local_guardians/',LocalGuardianListView.as_view(),name='view-all'),
    url(r'^api/genders/',GenderListView.as_view(),name='view-all'),
    url(r'^api/semesters/',SemesterListView.as_view(),name='view-all'),
    url(r'^api/sections/',SectionListView.as_view(),name='view-all'),
    url(r'^api/electiveslots/',ElectiveSlotListView.as_view(),name='view-all'),
    url(r'^api/addonslots/',AddonSlotListView.as_view(),name='view-all'),

]