
from django.db import models
from datetime import date
from utility.models import *

# Create your models here.
class Course(models.Model):

	# DATABASE FIELDS
	course_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)
	l = models.IntegerField()
	t = models.IntegerField()
	p = models.IntegerField()
	credits = models.IntegerField()
	department = models.ForeignKey(Department,on_delete=models.CASCADE)


	# META CLASS
	class Meta:
		verbose_name = 'Course'
		verbose_name_plural = 'Courses'

	# TO STRING METHOD
	def __str__(self):
		return (self.course_id) + "-" + (self.name) + "-" + str(self.department.department_id)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

# Create your models here.
class CourseType(models.Model):

	# DATABASE FIELDS
	type_id = models.CharField(max_length=10,primary_key=True)
	name = models.CharField(max_length=200)
	# META CLASS
	class Meta:
		verbose_name = 'Course Type'
		verbose_name_plural = 'Course Types'

	# TO STRING METHOD
	def __str__(self):
		return  (self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


# Create your models here.
class CoreCourseOffered(models.Model):

	# DATABASE FIELDS
	course = models.ForeignKey(Course,on_delete=models.CASCADE)
	batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
	session = models.ForeignKey(Session, on_delete=models.CASCADE)
	semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
	active = models.BooleanField(default = True)

	# META CLASS
	class Meta:
		verbose_name = 'Core Course Offered'
		verbose_name_plural = 'Core Courses Offered'
		unique_together = ('course', 'batch','session')

	# TO STRING METHOD
	def __str__(self):
		return str(self.course) + "-" + str(self.batch) + "-" + str(self.session) + "-" + str(self.semester)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


# Create your models here.
class ElectiveCourseOffered(models.Model):

	# DATABASE FIELDS
	course = models.ForeignKey(Course,on_delete=models.CASCADE)
	batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
	session = models.ForeignKey(Session, on_delete=models.CASCADE)
	semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
	active = models.BooleanField(default = True)
	slot = models.ForeignKey(ElectiveSlot, on_delete=models.CASCADE)
	basket = models.ForeignKey(ElectiveBasket, on_delete=models.CASCADE)
	seats = models.IntegerField()

	# META CLASS
	class Meta:
		verbose_name = 'Elective Course Offered'
		verbose_name_plural = 'Elective Courses Offered'
		unique_together = ('course', 'batch','session')

	# TO STRING METHOD
	def __str__(self):
		return str(self.course) + "-" + str(self.batch) + "-" + str(self.session) + "-" + str(self.semester)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.


class AddonCourseOffered(models.Model):

	# DATABASE FIELDS
	course = models.ForeignKey(Course,on_delete=models.CASCADE)
	batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
	session = models.ForeignKey(Session, on_delete=models.CASCADE)
	semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
	active = models.BooleanField(default = True)
	slot = models.ForeignKey(AddonSlot, on_delete=models.CASCADE)
	basket = models.ForeignKey(AddonBasket, on_delete=models.CASCADE)
	seats = models.IntegerField()

	# META CLASS
	class Meta:
		verbose_name = 'Addon Course Offered'
		verbose_name_plural = 'Addon Courses Offered'
		unique_together = ('course', 'batch','session')

	# TO STRING METHOD
	def __str__(self):
		return str(self.course) + "-" + str(self.batch) + "-" + str(self.session) + "-" + str(self.semester)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
