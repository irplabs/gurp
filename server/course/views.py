# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render


from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from utility.models import*
from utility.serializers import*
from utility.views import*
from utility.authenticate import*
from utility.session import*
from student.models import*
from student.serializers import*
from course.models import*
from course.serializers import*
import csv

class CourseListView(ListAPIView):
	queryset = Course.objects.all()
	serializer_class = CourseSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = CourseSerializer(queryset,many=True)
		return Response(serializer.data)

class CourseDetailView(RetrieveAPIView):

	queryset = Course.objects.all()
	serializer_class = CourseSerializer

class CoreCourseOfferedListView(ListAPIView):
	queryset = CoreCourseOffered.objects.all()
	serializer_class = CoreCourseOfferedSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = CoreCourseOfferedSerializer(queryset,many=True)
		return Response(serializer.data)

class ElectiveCourseOfferedListView(ListAPIView):
	queryset = ElectiveCourseOffered.objects.all()
	serializer_class = ElectiveCourseOfferedSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = ElectiveCourseOfferedSerializer(queryset,many=True)
		return Response(serializer.data)


class AddonCourseOfferedListView(ListAPIView):
	queryset = AddonCourseOffered.objects.all()
	serializer_class = AddonCourseOfferedSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = AddonCourseOfferedSerializer(queryset,many=True)
		return Response(serializer.data)