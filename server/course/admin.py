# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import*
from import_export import resources


# Register your models here.

@admin.register(Course)
class CourseAdmin(ImportExportModelAdmin):
    pass

@admin.register(CourseType)
class CourseTypeAdmin(ImportExportModelAdmin):
    pass

@admin.register(CoreCourseOffered)
class CoreCourseOfferedAdmin(ImportExportModelAdmin):
    pass

@admin.register(ElectiveCourseOffered)
class ElectiveCourseOfferedAdmin(ImportExportModelAdmin):
    pass

@admin.register(AddonCourseOffered)
class AddonCourseOfferedAdmin(ImportExportModelAdmin):
    pass