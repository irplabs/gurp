# Generated by Django 2.1.5 on 2019-01-25 15:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('utility', '0016_section'),
        ('course', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CoreCourseOffered',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('batch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Batch')),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='course.Course')),
                ('semester', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Semester')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Session')),
            ],
            options={
                'verbose_name_plural': 'Core Courses Offered',
                'verbose_name': 'Core Course Offered',
            },
        ),
    ]
