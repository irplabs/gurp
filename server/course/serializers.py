from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from rest_framework import*
from utility.models import*
from utility.serializers import*
from course.models import *

class CourseSerializer(ModelSerializer):

	department = DepartmentSerializer(read_only=True)

	class Meta:
		model = Course
		fields = "__all__"

class CourseTypeSerializer(ModelSerializer):

	class Meta:
		model = CourseType
		fields = "__all__"

class CoreCourseOfferedSerializer(ModelSerializer):

	course = CourseSerializer(read_only=True)
	batch = BatchSerializer(read_only=True)
	session = SessionSerializer(read_only=True)
	semester = SemesterSerializer(read_only=True)


	class Meta:
		model = CoreCourseOffered
		fields = "__all__"

class ElectiveCourseOfferedSerializer(ModelSerializer):

	course = CourseSerializer(read_only=True)
	batch = BatchSerializer(read_only=True)
	session = SessionSerializer(read_only=True)
	semester = SemesterSerializer(read_only=True)
	slot = ElectiveSlotSerializer(read_only=True)
	basket = ElectiveBasketSerializer(read_only=True)


	class Meta:
		model = ElectiveCourseOffered
		fields = "__all__"

class AddonCourseOfferedSerializer(ModelSerializer):

	course = CourseSerializer(read_only=True)
	batch = BatchSerializer(read_only=True)
	session = SessionSerializer(read_only=True)
	semester = SemesterSerializer(read_only=True)
	slot = ElectiveSlotSerializer(read_only=True)
	basket = ElectiveBasketSerializer(read_only=True)


	class Meta:
		model = AddonCourseOffered
		fields = "__all__"