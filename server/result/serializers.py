from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from rest_framework import*
from utility.models import*
from utility.serializers import*
from course.models import *
from course.serializers import*
from result.models import*

class ComponentResultSerializer(ModelSerializer):


	class Meta:
		model = ComponentResult
		fields = "__all__"

class CourseResultSerializer(ModelSerializer):

	c1 = ComponentResultSerializer(read_only=True)
	c2 = ComponentResultSerializer(read_only=True)
	c3 = ComponentResultSerializer(read_only=True)

	class Meta:
		model = CourseResult
		fields = "__all__"

