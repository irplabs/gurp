from django.db import models
from datetime import date
from student.models import*
from utility.models import*
from decimal import Decimal

# Create your models here.
class ComponentResult(models.Model):

	# DATABASE FIELDS
	score = models.DecimalField(max_digits=8,decimal_places=2,blank=True,null=True)
	present = models.BooleanField(default = False)
	

	# META CLASS
	class Meta:
		verbose_name = 'Component Result'
		verbose_name_plural = 'Component Results'

	# TO STRING METHOD
	def __str__(self):
		return str(self.score) +"-"+ str(self.present)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

# Create your models here.
class CourseResult(models.Model):

	# DATABASE FIELDS
	c1 = models.ForeignKey(ComponentResult,on_delete=models.CASCADE,blank=True,null=True,related_name="C1")
	c2 = models.ForeignKey(ComponentResult,on_delete=models.CASCADE,blank=True,null=True,related_name="C2")
	c3 = models.ForeignKey(ComponentResult,on_delete=models.CASCADE,blank=True,null=True,related_name="C3")
	total = models.DecimalField(max_digits=8,decimal_places=2,blank=True,default=Decimal('0.00'))
	cgpi = models.DecimalField(max_digits=8,decimal_places=2,blank=True,default=Decimal('0.00'))
	

	# META CLASS
	class Meta:
		verbose_name = 'Course Result'
		verbose_name_plural = 'Course Results'

	# TO STRING METHOD
	def __str__(self):
		return str(self.c1) + " " + str(self.c2) + " " + str(self.c3) + " " + str(self.total) + " " + str(self.cgpi)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something




