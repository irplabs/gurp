from django.conf.urls import url
from django.urls import path, include
from student.views import*

urlpatterns = [

	
    url(r'^api/students/$',StudentListView.as_view(),name='view-all'),
    url(r'^api/students/(?P<pk>\w+)',StudentDetailView.as_view(),name='detail'),
    url(r'^api/enrolledcourses/$',EnrolledCourseListView.as_view(),name='view-all'),
    url(r'^api/droppedcourses/$',DropCourseListView.as_view(),name='view-all'),
    url(r'^api/semester_results/$',SemesterResultListView.as_view(),name='view-all'),


]