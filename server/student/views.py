# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render


from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView
from utility.models import*
from utility.serializers import*
from utility.views import*
from utility.authenticate import*
from utility.session import*
from student.models import*
from student.serializers import*

class StudentListView(ListAPIView):
	queryset = Student.objects.all()
	serializer_class = StudentDetailSerializer

	def list(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = StudentDetailSerializer(queryset,many=True)
		return Response(serializer.data)

class StudentDetailView(RetrieveAPIView):

	queryset = Student.objects.all()
	serializer_class = StudentSerializer



class EnrolledCourseListView(APIView):

	def get(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})

		filters = {}

		course_id = request.GET.get('course_id')
		if course_id:
			filters['course'] = Course.objects.get(course_id=course_id)

		admission_id = request.GET.get('admission_id')
		if admission_id :
			filters['student'] = Student.objects.get(admission_id=admission_id)

		session = get_session(request)

		session_id = request.GET.get('session_id')
		if session_id:
			filters['session'] = Session.objects.get(session_id=session_id)

		enrolled = EnrolledCourse.objects.filter(**filters)
		serializer = EnrolledCourseSerializer(enrolled,many=True)
		return Response(serializer.data)

class DropCourseListView(APIView):

	def get(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})

		# filters = {}

		# course_id = request.GET.get('course_id')
		# if course_id:
		# 	filters['course'] = Course.objects.get(course_id=course_id)

		# admission_id = request.GET.get('admission_id')
		# if admission_id :
		# 	filters['student'] = Student.objects.get(admission_id=admission_id)

		# session = get_session(request)

		# session_id = request.GET.get('session_id')
		# if session_id:
		# 	filters['session'] = Session.objects.get(session_id=session_id)

		dropped = DropCourse.objects.all().order_by('enrolled__student__admission_id')
		serializer = DropCourseSerializer(dropped,many=True)
		return Response(serializer.data)

class SemesterResultListView(APIView):

	def get(self,request):

		# if not isAuthenticated(request):
		# 	return Response({'STATUS':'FORBIDDEN'})

		# filters = {}

		# course_id = request.GET.get('course_id')
		# if course_id:
		# 	filters['course'] = Course.objects.get(course_id=course_id)

		# admission_id = request.GET.get('admission_id')
		# if admission_id :
		# 	filters['student'] = Student.objects.get(admission_id=admission_id)

		# session = get_session(request)

		# session_id = request.GET.get('session_id')
		# if session_id:
		# 	filters['session'] = Session.objects.get(session_id=session_id)

		results = SemesterResult.objects.all().order_by('student__admission_id')
		serializer = SemesterResultSerializer(results,many=True)
		return Response(serializer.data)