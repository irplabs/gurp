from django.db import models
from django.contrib.auth.models import User
from utility.models import*
from course.models import*
from result.models import*

# Create your models here.

class Student(models.Model):

	# DATABASE FIELDS
	user = models.OneToOneField(User, on_delete=models.CASCADE,blank=True)
	admission_id = models.CharField(max_length=20,primary_key=True)
	person = models.ForeignKey(Person,on_delete=models.CASCADE)
	address = models.ForeignKey(Address,on_delete=models.CASCADE,blank=True,null=True)
	guardian = models.ForeignKey(Guardian,on_delete=models.CASCADE,blank=True,null=True)
	local_guardian = models.ForeignKey(LocalGuardian,on_delete=models.CASCADE,blank=True,null=True)
	gender = models.ForeignKey(Gender,on_delete=models.CASCADE)
	category = models.ForeignKey(Category,on_delete=models.CASCADE)
	birth_date = models.DateField()
	aadhar = models.CharField(max_length=20,blank=True)
	highschool = models.ForeignKey(HighschoolRecord, on_delete=models.CASCADE,blank=True,null=True)
	intermediate = models.ForeignKey(IntermediateRecord, on_delete=models.CASCADE,blank=True,null=True)
	qualifying_exam = models.ForeignKey(QualifyingExam, on_delete=models.CASCADE,blank=True,null=True)
	batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
	semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
	section = models.ForeignKey(Section, on_delete=models.CASCADE)
	active = models.BooleanField(default = True)
	photograph = models.ImageField(upload_to = 'media/images/students/')

	# META CLASS
	class Meta:
		verbose_name = 'Student'
		verbose_name_plural = 'Students'

	# TO STRING METHOD
	def __str__(self):
		return (self.admission_id) + "-" + (self.person.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class EnrolledCourse(models.Model):
	student = models.ForeignKey(Student,on_delete=models.CASCADE)
	course = models.ForeignKey(Course,on_delete=models.CASCADE)
	course_type = models.ForeignKey(CourseType,on_delete=models.CASCADE)
	session = models.ForeignKey(Session, on_delete=models.CASCADE)
	semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
	result = models.ForeignKey(CourseResult, on_delete=models.CASCADE)
	date = models.DateField()
	# META CLASS
	class Meta:
		verbose_name = 'Enrolled Course'
		verbose_name_plural = 'Enrolled Courses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.student) + "-" + str(self.course) + "-" +str(self.result)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class DropCourse(models.Model):
	enrolled = models.ForeignKey(EnrolledCourse,on_delete=models.CASCADE)
	reason = models.ForeignKey(DropReason,on_delete=models.CASCADE)
	date = models.DateField()
	# META CLASS
	class Meta:
		verbose_name = 'Dropped Course'
		verbose_name_plural = 'Dropped Courses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.enrolled.student) + "-" + str(self.enrolled.course) + "-" +str(self.reason)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class MakeUpCourse(models.Model):
	enrolled = models.ForeignKey(EnrolledCourse,on_delete=models.CASCADE)
	old_score = models.DecimalField(max_digits=8,decimal_places=2,blank=True)
	date = models.DateField()
	# META CLASS
	class Meta:
		verbose_name = 'MakeUp Course'
		verbose_name_plural = 'MakeUp Courses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.enrolled.student) + "-" + str(self.enrolled.course) + "-" +str(self.old_score)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something


# Create your models here.
class SemesterResult(models.Model):

	# DATABASE FIELDS
	student = models.ForeignKey(Student,on_delete=models.CASCADE)
	session = models.ForeignKey(Session, on_delete=models.CASCADE)
	semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
	credits = models.IntegerField()
	addon_credits = models.IntegerField()
	sgpi = models.DecimalField(max_digits=8,decimal_places=2,blank=True,default=Decimal('0.00'))
	rank = models.IntegerField(blank=True)
	result_date = models.DateField()
	fingerprint = models.CharField(max_length=100,blank=True)


	# META CLASS
	class Meta:
		verbose_name = 'Semester Result'
		verbose_name_plural = 'Semester Results'
		unique_together = ('student', 'semester','session')

	# TO STRING METHOD
	def __str__(self):
		return str(self.student) + " " + str(self.semester) + " " + str(self.sgpi) 

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

