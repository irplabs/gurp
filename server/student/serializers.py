from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from rest_framework import*
from utility.models import*
from utility.serializers import*
from course.serializers import*
from result.models import*
from result.serializers import*
from student.models import*

class StudentSerializer(ModelSerializer):

	person = PersonSerializer(read_only=True)
	address = AddressSerializer(read_only=True)
	guardian = GuardianSerializer(read_only=True)
	local_guardian = LocalGuardianSerializer(read_only=True)
	gender = GenderSerializer(read_only=True)
	category = CategorySerializer(read_only=True)
	highschool = HighschoolRecordSerializer(read_only=True)
	intermediate = IntermediateRecordSerializer(read_only=True)
	qualifying_exam = QualifyingExamSerializer(read_only=True)
	batch = BatchSerializer(read_only=True)
	semester = SemesterSerializer(read_only=True)
	section = SectionSerializer(read_only=True)


	class Meta:
		model = Student
		fields = "__all__"

class StudentDetailSerializer(ModelSerializer):

	person = PersonSerializer(read_only=True)

	class Meta:
		model = Student
		fields = ('admission_id','person')

class EnrolledCourseSerializer(ModelSerializer):

	student = StudentDetailSerializer(read_only=True)
	course = CourseSerializer(read_only=True)
	course_type = CourseTypeSerializer(read_only=True)
	session = SessionSerializer(read_only=True)
	semester = SemesterSerializer(read_only=True)
	result = CourseResultSerializer(read_only=True)

	class Meta:
		model = EnrolledCourse
		fields = "__all__"

class DropCourseSerializer(ModelSerializer):

	enrolled =EnrolledCourseSerializer(read_only=True)
	reason = DropReasonSerializer(read_only=True)

	class Meta:
		model = DropCourse
		fields = "__all__"

class SemesterResultSerializer(ModelSerializer):

	student = StudentDetailSerializer(read_only=True)
	session = SessionSerializer(read_only=True)
	semester = SemesterSerializer(read_only=True)

	class Meta:
		model = SemesterResult
		fields = "__all__"