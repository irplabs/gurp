# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import*
from import_export import resources


# Register your models here.

@admin.register(Student)
class StudentAdmin(ImportExportModelAdmin):
    pass

@admin.register(EnrolledCourse)
class EnrolledCourseAdmin(ImportExportModelAdmin):
    pass

@admin.register(DropCourse)
class DropCourseAdmin(ImportExportModelAdmin):
    pass

@admin.register(MakeUpCourse)
class MakeUpCourseAdmin(ImportExportModelAdmin):
    pass

@admin.register(SemesterResult)
class SemesterResultAdmin(ImportExportModelAdmin):
    pass