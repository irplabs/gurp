# Generated by Django 2.1.5 on 2019-02-01 10:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0007_dropcourse'),
    ]

    operations = [
        migrations.CreateModel(
            name='MakeUpCourse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('old_score', models.DecimalField(blank=True, decimal_places=2, max_digits=8)),
                ('date', models.DateField()),
                ('enrolled', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='student.EnrolledCourse')),
            ],
            options={
                'verbose_name_plural': 'MakeUp Courses',
                'verbose_name': 'MakeUp Course',
            },
        ),
    ]
