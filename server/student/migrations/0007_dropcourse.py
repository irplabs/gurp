# Generated by Django 2.1.5 on 2019-01-31 16:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('utility', '0019_auto_20190131_1640'),
        ('student', '0006_auto_20190130_0957'),
    ]

    operations = [
        migrations.CreateModel(
            name='DropCourse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('enrolled', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='student.EnrolledCourse')),
                ('reason', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.DropReason')),
            ],
            options={
                'verbose_name': 'Dropped Course',
                'verbose_name_plural': 'Dropped Courses',
            },
        ),
    ]
