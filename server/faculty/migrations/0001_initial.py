# Generated by Django 2.1.5 on 2019-01-30 11:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('utility', '0017_addonbasket_addonslot_electivebasket_electiveslot'),
    ]

    operations = [
        migrations.CreateModel(
            name='Faculty',
            fields=[
                ('faculty_id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('active', models.BooleanField(default=True)),
                ('interest', models.CharField(max_length=200)),
                ('photograph', models.ImageField(upload_to='media/images/faculties/')),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Department')),
                ('gender', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Gender')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utility.Person')),
                ('user', models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Faculties',
                'verbose_name': 'Faculty',
            },
        ),
    ]
