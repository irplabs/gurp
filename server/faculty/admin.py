# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from faculty.models import*
from import_export import resources


# Register your models here.

@admin.register(Faculty)
class FacultyAdmin(ImportExportModelAdmin):
    pass

@admin.register(FacultyCourse)
class FacultyCourseAdmin(ImportExportModelAdmin):
    pass