from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from rest_framework import*
from utility.models import*
from utility.serializers import*
from course.serializers import*
from result.models import*
from result.serializers import*
from student.models import*
from student.serializers import*
from faculty.models import*


class FacultySerializer(ModelSerializer):

	person = PersonSerializer(read_only=True)
	gender = GenderSerializer(read_only=True)
	department = DepartmentSerializer(read_only=True)

	class Meta:
		model = Faculty
		fields = "__all__"

class FacultyCourseSerializer(ModelSerializer):

	course = CourseSerializer(read_only=True)
	cordinator = FacultySerializer(read_only=True)
	faculty = FacultySerializer(read_only=True)
	session = SessionSerializer(read_only=True)
	section = SectionSerializer(read_only=True)

	class Meta:
		model = FacultyCourse
		fields = "__all__"